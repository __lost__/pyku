""" Generate a pickled word dictionary for pyku.py.

    Usage:
        python process.py word_list_path parts_of_speech

    where:
        word_list_path: path to a word list from SCOWL, [1]
        parts_of_speech: path to a parts of speech .txt file,
        see "Other Artifacts" at [1].

    This creates the file word_list_path + '.pkl'. See pyku.py's
    docstring for the format.

[1] Spell Checker Oriented Word Lists, see wordlist.aspell.net

"""

import re, pickle, sys

def count_syllables(word):
    """ This is pretty crude! """
    word = word.lower()
    p = re.compile("[aeiouy]+")
    # count number of vowel groups:
    n_vowel_groups = len(p.findall(word))
    # remove last group if word ends in 'e':
    if word.endswith('e'):
        n_vowel_groups -= 1
    # all words are at least 1 syllable:
    return n_vowel_groups if n_vowel_groups > 1 else 1

def main():    
    word_list_path, pos_path = sys.argv[1:]
    pos_dict = {}
    word_dict = {}
    
    cat = uncat = 0
    
    with open(pos_path, 'r') as d:
        for line in d:
            phrase, pos_tag = line.strip().split('\t')
            pos_dict[phrase] = pos_tag

    with open(word_list_path, 'r') as wl:
        for line in wl:
            word = line.strip()
            if word in pos_dict:
                # only take the first-listed pos, and remove any '|'
                pos = pos_dict[word].replace('|', '')[0]
                # for intransitive verbs, add an s:
                if pos == 'i':
                    word = word + 's'
                ns = count_syllables(word)
                if pos not in word_dict:
                    word_dict[pos] = []
                word_dict[pos].append((word, ns))
                #print word, pos, ns
                cat += 1
            else:
                #print word, '-'
                uncat += 1

        print '%i categorised:' % cat
        for p, wc in word_dict.items():
            print p, len(wc)
        print '%i uncategorised' % uncat
        with open(word_list_path + '.pkl', 'w') as pkl:
            pickle.dump(word_dict, pkl)
        
""" older stuff                
            # only take the first pos-tag, and remove any '|'
            pos_tag = pos_tag.replace('|', '')[0]
            if pos_tag == 'N':
                pos_dict[phrase] = 'noun'
            elif pos_tag == '


            # discard any proper nouns, acrnomys etc:
            if phrase.lower() != phrase:
                continue           
            # discard any hyphenated words
            if '-' in phrase:
                continue

            # count noun-phrases as nouns:
            if pos_tag == 'h':
                pos_tag = 'N'
            words = phrase.split(' ')
            n_syl = sum(count_syllables(w) for w in words)
            if pos_tag not in pos_dict:
                 pos_dict[pos_tag] = []
            pos_dict[pos_tag].append((phrase, n_syl))
            n += 1
    
"""
if __name__ == '__main__':
    main()
