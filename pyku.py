""" Randomly generate a haiku.

    Usage:
        python pyku.py wordfile
    
    where `wordfile` is a path to a pickled dictionary with:
	    key:=str defining part of speech, see POS tag used in [1], e.g.
            "i":  intransitive verb
            "N":  noun
            "A":  adjective
        value:=sequence (str word, int number of syllables)
    
    english-words.10.pkl is an example wordfile.

    Note `process.py` can be used to generate such a wordfile.

[1] wordlist.aspell.net/pos-readme

"""

import sys, pickle, random

_lines = (5, 7, 5)

# define lines/syllables/parts of speech in a haiku:
#PATTERN = (('D', 'A', 'N', 'i', 5),
#           ('P', 'D', 'N', 'N', 7),
#           ('A', 'N', 5))
PATTERN = (('D', 'A', 'N', 5),
           ('i', 'P', 'D', 'N', 7),
           ('D', 'A', 'N', 5)) 

def haiku(language):
    """ Returns a multiline str with a haiku.

        Args:
            language: dict, see module docstr.
    """

    current = []
    for line in PATTERN:
        nsyl = 0
        while nsyl != line[-1]:
            nsyl = 0
            words = []
            for pos in line[:-1]:
                npos = len(language[pos])
                word, ns = language[pos][random.randint(0, npos-1)]
                words.append(word)
                nsyl += ns
        current.append(' '.join(words))
    return '\n'.join(current)

if __name__ == '__main__':
    lang_path = sys.argv[1]
    with open(lang_path, 'r') as pkl:
        lang = pickle.load(pkl)
    
    while True:
        print haiku(lang)
        raw_input()

